Il s'agit d'un projet réalisé dans le cadre de ma licence informatique.

C'est une application IHM Java qui met en oeuvre une architecture MVC (Model View Controller) avec l'utilisation de Listeneurs et d'une base de données rudimentaire.

EXECUTION :
- executez le fichier "MainFrame.java" présent dans le dossier "view".